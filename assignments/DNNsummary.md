## What is Deep Neural Network?
A deep neural network is a neural network with a certain level of complexity, a neural network with more than two layers. Deep neural networks use sophisticated mathematical modeling to process data in complex ways.

![](https://orbograph.com/wp-content/uploads/2019/01/DeepLearn.png)

We can use the Imagenet, a repository of millions of digital images to classify a dataset into categories like cats and dogs. DL nets are increasingly used for dynamic images apart from static ones and for time series and text analysis.

Training the data sets forms an important part of Deep Learning models. In addition, Backpropagation is the main algorithm in training DL models.
Neural networks are functions that have inputs like x1,x2,x3…that are transformed to outputs like z1,z2,z3 and so on in two (shallow networks) or several intermediate operations also called layers (deep networks).

The weights and biases change from layer to layer. ‘w’ and ‘v’ are the weights or synapses of layers of the neural networks.

![](https://www.tutorialspoint.com/python_deep_learning/images/backpropagation_algorithm.jpg)

![](https://www.tutorialspoint.com/python_deep_learning/images/dl_mapping.jpg)

The best use case of deep learning is the supervised learning problem.

## Deep Nets and Shallow Nets
There is no clear threshold of depth that divides shallow learning from deep learning; but it is mostly agreed that for deep learning which has multiple non-linear layers, CAP must be greater than two. Basic node in a neural net is a perception mimicking a neuron in a biological neural network. Then we have multi-layered Perception or MLP. Each set of inputs is modified by a set of weights and biases; each edge has a unique weight and each node has a unique bias.

The accuracy of correct prediction has become so accurate that recently at a Google Pattern Recognition Challenge, a deep net beat a human.

## Choosing a Deep Net

Consider the following points while choosing a deep net −

    For text processing, sentiment analysis, parsing and name entity recognition, we use a recurrent net or RNTN;

    For any language model that operates at character level, we use the recurrent net.

    For image recognition, we use deep belief network DBN or convolutional network.

    For object recognition, we use a RNTN or a convolutional network.

    For speech recognition, we use recurrent net.


