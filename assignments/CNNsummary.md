### What is Convolutional Neural Network (CNN)?
A convolutional neural network (CNN) is a specific type of artificial neural network that uses perceptrons, a machine learning unit algorithm, for supervised learning, to analyze data. CNNs apply to image processing, natural language processing and other kinds of cognitive tasks.
![](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/75_blog_image_1.png)

Here is an example to depict how CNN recognizes an image:

![](https://www.simplilearn.com/ice9/free_resources_article_thumb/array_of_pixel_values.png)

### Layers in a Convolutional Neural Network
A convolution neural network has multiple hidden layers that help in extracting information from an image. The four important layers in CNN are:

    Convolution layer
    ReLU layer
    Pooling layer
    Fully connected layer

## Convolution Layer
This is the first step in the process of extracting valuable features from an image. A convolution layer has several filters that perform the convolution operation. Every image is considered as a matrix of pixel values.

## ReLU layer
ReLU stands for the rectified linear unit. Once the feature maps are extracted, the next step is to move them to a ReLU layer. 

ReLU performs an element-wise operation and sets all the negative pixels to 0. It introduces non-linearity to the network, and the generated output is a rectified feature map. Below is the graph of a ReLU function:

![](https://www.simplilearn.com/ice9/free_resources_article_thumb/ReLU_layer.png)

## Pooling Layer
Pooling is a down-sampling operation that reduces the dimensionality of the feature map. The rectified feature map now goes through a pooling layer to generate a pooled feature map.
![](https://s3.amazonaws.com/static2.simplilearn.com/ice9/free_resources_article_thumb/Pooling_filters.png)


Here’s how exactly CNN recognizes a bird:

    The pixels from the image are fed to the convolutional layer that performs the convolution operation 
    It results in a convolved map 
    The convolved map is applied to a ReLU function to generate a rectified feature map 
    The image is processed with multiple convolutions and ReLU layers for locating the features 
    Different pooling layers with various filters are used to identify specific parts of the image 
    The pooled feature map is flattened and fed to a fully connected layer to get the final output

![](https://www.simplilearn.com/ice9/free_resources_article_thumb/CNN_recognizes_a_bird1.png)